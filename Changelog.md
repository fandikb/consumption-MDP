# ChangeLog for [FiMDP](https://github.com/xblahoud/FiMDP) (Fuel in MDP) project

## [1.0.2]

### Added
 * data structures for *consumption Markov Decision Processes (CMDP)*
 * Solvers for strategy synthesis (and solving underlying decision problems) for Survival, Positive reachability 
 (of given target set $`T`$), almost-sure reachability of $`T`$, and almsot-sure Büchi objective on $`T`$
 * Algorithm for explicit representation of energy + MEC decomposition of this explicit MDP
 * example notebooks

[Unpublished]: https://github.com/xblahoud/FiMDP/compare/v1.0.2..HEAD
[1.0.2]: https://github.com/xblahoud/FiMDP/tree/v1.0.2